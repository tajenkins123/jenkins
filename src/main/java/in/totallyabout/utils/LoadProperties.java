package in.totallyabout.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoadProperties {

	public static InputStream inStream;
	public static Properties configProperties;

	static {
		configProperties = new Properties();
		try {
			inStream = new FileInputStream("src/main/resources/config.properties");
			configProperties.load(inStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
