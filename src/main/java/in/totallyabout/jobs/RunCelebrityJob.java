package in.totallyabout.jobs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.StringUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.totallyabout.beans.BasicInfoBean;
import in.totallyabout.beans.CelebrityBean;
import in.totallyabout.beans.SocialMappingBean;
import in.totallyabout.utils.LoadProperties;

public class RunCelebrityJob {

	private static final String taDataFileName = LoadProperties.configProperties.getProperty("taDataFileName");
	private static final String celebrityBulkCreateApi = LoadProperties.configProperties.getProperty("celebrityBulkCreateApi");
	private static final String celebrityBulkUpdateApi = LoadProperties.configProperties.getProperty("celebrityBulkUpdateApi");
	private static final String celebritiesApi = LoadProperties.configProperties.getProperty("celebritiesApi");
	private static final String invalidurls = LoadProperties.configProperties.getProperty("invalidurls");
	private static String twitterBaseUrl = "https://twitter.com/";
    private static String facebookBaseUrl = "https://www.facebook.com/";

	public static void main(String[] args) throws ClientProtocolException, IOException {

		List<CelebrityBean> celebList = new ArrayList<CelebrityBean>();
		Map<String,String> urls = new HashMap<String, String>();
		ClassLoader classLoader = new RunCelebrityJob().getClass().getClassLoader();
		FileInputStream excelFile;
		Workbook workbook;

		try {
			excelFile = new FileInputStream(classLoader.getResource(taDataFileName).getFile());
			workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> rowItr = datatypeSheet.iterator();
			
			if(rowItr.hasNext() == false){
				System.out.println("No data found in the given excel sheet!!!");
				return;
			}
			
			while (rowItr.hasNext()) {

				CelebrityBean celebrityBean = new CelebrityBean();
				SocialMappingBean socialMappingBean = new SocialMappingBean();
				BasicInfoBean basicInfoBean = new BasicInfoBean();
				Row row = rowItr.next();

				if(row != null){
				//Skipping row 0, as it contains field names
				/*if (row.getRowNum() == 0)
					continue;*/
				
				System.out.println ("Row No.: " + row.getRowNum ()+" Celeb: "+row.getCell(1).getStringCellValue());
				urls.put(row.getCell(1).getStringCellValue()+" twitter account", twitterBaseUrl+row.getCell(2).getStringCellValue());
				urls.put(row.getCell(1).getStringCellValue()+" fb account", facebookBaseUrl+row.getCell(3).getStringCellValue());

				celebrityBean.setId((int)row.getCell(0).getNumericCellValue());
				celebrityBean.setDisplayName(row.getCell(1).getStringCellValue());
				socialMappingBean.setTwitterId(row.getCell(2).getStringCellValue());
				socialMappingBean.setFacebookId(row.getCell(3).getStringCellValue());
				socialMappingBean.setInstagramId(row.getCell(4).getStringCellValue());
				socialMappingBean.setWikiid(row.getCell(5).getStringCellValue());
				socialMappingBean.setYoutubeSearchTerms(row.getCell(6).getStringCellValue());
				celebrityBean.setSocialMappingIds(socialMappingBean);
				celebrityBean.setSearchTerms(row.getCell(7).getStringCellValue());
				celebrityBean.setImageUrl(row.getCell(8).getStringCellValue());
				basicInfoBean.setBornPlace(row.getCell(9).getStringCellValue());
				celebrityBean.setBasicInfo(basicInfoBean);
				celebrityBean.setCategory(row.getCell(10).getStringCellValue());
				celebrityBean.setRating((int)row.getCell(11).getNumericCellValue());
				celebList.add(celebrityBean);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//sendCelebrityBulkCreateCall(celebList);
		sendCelebrityBulkUpdateCall(celebList);
		//findInvalidUrl(urls);
	}
	
	public static void sendCelebrityBulkCreateCall(List<CelebrityBean> celebListInBulk) throws ClientProtocolException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		String celebJsonInString = mapper.writeValueAsString(celebListInBulk);
		System.out.println("celebJsonInString "+celebJsonInString);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(celebrityBulkCreateApi);
        httpPut.addHeader("Content-Type","application/json");
        StringEntity jsonData = new StringEntity(celebJsonInString);
        httpPut.setEntity(jsonData);        
		HttpResponse response = httpClient.execute(httpPut);
		
		if(response!=null)
		   System.out.println("Response: "+response.toString());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "+ response.getStatusLine().getStatusCode());
		}
	}
	
public static void sendCelebrityBulkUpdateCall(List<CelebrityBean> celebListInBulk) throws ClientProtocolException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		String celebJsonInString = mapper.writeValueAsString(celebListInBulk);
		System.out.println(celebJsonInString);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPut = new HttpPost(celebrityBulkUpdateApi);
        httpPut.addHeader("Content-Type","application/json");
        StringEntity jsonData = new StringEntity(celebJsonInString);
        httpPut.setEntity(jsonData);        
		HttpResponse response = httpClient.execute(httpPut);
		
		if(response!=null)
		   System.out.println("Response: "+response.toString());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "+ response.getStatusLine().getStatusCode());
		}
	}
	
	
    public static void findInvalidUrl(Map<String,String> urls) throws ClientProtocolException, IOException{
		
		ObjectMapper mapper = new ObjectMapper();
		String urlsJsonInString = mapper.writeValueAsString(urls);
		System.out.println(urlsJsonInString);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(invalidurls);
        httpPut.addHeader("Content-Type","application/json");
        StringEntity jsonData = new StringEntity(urlsJsonInString);
        httpPut.setEntity(jsonData);        
		HttpResponse response = httpClient.execute(httpPut);
		
		if(response!=null){
			String responseAsString = org.apache.http.util.EntityUtils.toString(response.getEntity());
			System.out.println(responseAsString);
		}else{
			System.out.println(response);
		}
			
	}
	
	public static void sendCelebrityDetalsRequest(String collectionName) throws ClientProtocolException, IOException{
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(celebritiesApi+"/"+collectionName);
		HttpResponse response = httpClient.execute(httpGet);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "+ response.getStatusLine().getStatusCode());
		}
		System.out.println();
	}
}
