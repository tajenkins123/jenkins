package in.totallyabout.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CelebrityBean {
	
	private int id;
	private String displayName;
	private SocialMappingBean socialMappingIds;
	private String searchTerms;
	private String imageUrl;
	private String category;
	private BasicInfoBean basicInfo;
	private Integer rating;
	Boolean updateESNameAndRating = true;

	
	public Boolean getUpdateESNameAndRating() {
		return updateESNameAndRating;
	}
	public void setUpdateESNameAndRating(Boolean updateESNameAndRating) {
		this.updateESNameAndRating = updateESNameAndRating;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public SocialMappingBean getSocialMappingIds() {
		return socialMappingIds;
	}
	public void setSocialMappingIds(SocialMappingBean socialMappingIds) {
		this.socialMappingIds = socialMappingIds;
	}
	public String getSearchTerms() {
		return searchTerms;
	}
	public void setSearchTerms(String searchTerms) {
		this.searchTerms = searchTerms;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public BasicInfoBean getBasicInfo() {
		return basicInfo;
	}
	public void setBasicInfo(BasicInfoBean basicInfo) {
		this.basicInfo = basicInfo;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
}
