package in.totallyabout.beans;

public class BasicInfoBean {

	private String bornPlace;

	public String getBornPlace() {
		return bornPlace;
	}

	public void setBornPlace(String bornPlace) {
		this.bornPlace = bornPlace;
	}
}
