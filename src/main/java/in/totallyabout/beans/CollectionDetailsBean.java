package in.totallyabout.beans;

public class CollectionDetailsBean {

	private int count;
	private String maxId;
	private String minId;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getMaxId() {
		return maxId;
	}
	public void setMaxId(String maxId) {
		this.maxId = maxId;
	}
	public String getMinId() {
		return minId;
	}
	public void setMinId(String minId) {
		this.minId = minId;
	}
}
