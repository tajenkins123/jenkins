package in.totallyabout.beans;

public class SocialMappingBean {

	private String twitterId;
	private String facebookId;
	private String instagramId;
	private String wikiid;
	private String youtubeSearchTerms;
	
	public String getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}
	public String getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	public String getInstagramId() {
		return instagramId;
	}
	public void setInstagramId(String instagramId) {
		this.instagramId = instagramId;
	}
	public String getWikiid() {
		return wikiid;
	}
	public void setWikiid(String wikiid) {
		this.wikiid = wikiid;
	}
	public String getYoutubeSearchTerms() {
		return youtubeSearchTerms;
	}
	public void setYoutubeSearchTerms(String youtubeSearchTerms) {
		this.youtubeSearchTerms = youtubeSearchTerms;
	}	
}
